import { post } from "../api";

/**
 * Sets a node to be published
 *
 * @param {string} uuid
 * @param {string} language
 */
export async function publishNode(uuid, language) {
  post(`/fund/nodes/${uuid}/languages/${language}/published`, {});
}
