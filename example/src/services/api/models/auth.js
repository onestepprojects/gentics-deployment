import { get_base } from "../api";

/**
 * Gets current logged in user
 *
 * @returns {Promise}
 */
export async function getCurrentUser() {
  return get_base(`/auth/me`);
}
