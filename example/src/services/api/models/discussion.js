import { v4 as uuid } from "uuid";

import { graphQl, post } from "../api";

/**
 * Returns a list of comments for a particular
 * discussion board with id
 *
 * @param {string} id  unique id
 * @returns {Promise} response for api call
 */
export async function getComments(id) {
  return graphQl(`{
    nodes(filter: {
      and: [{
        schema: {
          is: discussion
        },
        fields: {
          discussion: {
            id: {
              equals: "${id}"
            }
          }
        }
      }]
    }) {
    elements {
      uuid
        ... on discussion {
        fields {
          comments {
            uuid
            ... on comment {
              created
              creator {
                uuid
                firstname
                lastname
                username
              }
              fields {
                value
              }
            }
          }
        }
      } 
    }
  }
  }`);
}

/**
 * Creates (or updates) a new discussion board with id and name
 *
 * @param {string} id   unique id
 * @param {string} name
 * @returns {Promise} uuid and language of discussion board
 */
export async function newDiscussion(id, name) {
  return post(`/webroot/${id}`, {
    language: "en",
    schema: {
      name: "discussion",
    },
    fields: {
      id: id,
      name: name,
    },
  }).then((data) => {
    return { uuid: data.uuid, language: data.language };
  });
}

/**
 * Creates a new comment from text
 *
 * @param {string} text value of comment
 * @returns {Promise} uuid and language of new comment
 */
export async function newPost(text) {
  var id = uuid();

  return post(`/webroot/comments/${id}`, {
    language: "en",
    schema: {
      name: "comment",
    },
    fields: {
      id: id,
      value: text,
    },
  }).then((data) => {
    return { uuid: data.uuid, language: data.language };
  });
}

/**
 * Attaches post to discussion board
 *
 * @param {string} uuid       unique id of comment
 * @param {string} boardUuid  unique id of discussion board
 * @param {string} comments   current list of board comments
 * @returns {Promise}
 */
export async function addPostToDiscussion(uuid, boardUuid, comments) {
  if (!comments) {
    comments = [{ uuid: uuid }];
  } else {
    comments = [...comments, { uuid: uuid }];
  }

  return post(`/nodes/${boardUuid}`, {
    language: "en",
    fields: {
      comments: comments,
    },
  });
}
