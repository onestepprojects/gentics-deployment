/* eslint-disable no-undef */

const apiUrl = `${process.env.REACT_APP_GENTICS_API_URL}/api/v2`;
const projectName = "fund";

export function graphQl(query, variables) {
  return post(`/graphql`, {
    query,
    variables,
  }).then((response) => response.data);
}

export function get(path, headers = {}) {
  return fetch(`${apiUrl}/${projectName}${path}`, {
    headers: headers,
  }).then((response) => response.json());
}

export function get_base(path, headers = {}) {
  return fetch(`${apiUrl}${path}`, {
    headers: headers,
  }).then((response) => response.json());
}

export function post(path, data) {
  return fetch(`${apiUrl}/${projectName}${path}`, {
    body: JSON.stringify(data),
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
}
