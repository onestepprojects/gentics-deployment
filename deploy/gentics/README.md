# Gentics Mesh on EKS

## Prerequisites:
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
- [Docker](https://docs.docker.com/docker-for-mac/install/)
- Required IAM permissions - this example assumes that you have logged in as a root user.


1. Create your Amazon EKS cluster

   1. Create VPC and Subnets
   ```shell
    aws cloudformation create-stack \
        --stack-name gmesh-vpc-stack \
        --template-url https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-private-subnets.yaml \
        --tags Key=application,Value=onesteprelief-solutions
    ```
   2. Creat IAM Role
   ```shell
   aws iam create-role \
    --role-name gmeshEKSClusterRole \
    --assume-role-policy-document file://"cluster-role-trust-policy.json"
   ```

   3. Attach the required Amazon EKS managed IAM policy to the role.
   ```shell
   aws iam attach-role-policy \
    --policy-arn arn:aws:iam::aws:policy/AmazonEKSClusterPolicy \
    --role-name gmeshEKSClusterRole
   ```

   4. Open the Amazon EKS console at https://console.aws.amazon.com/eks/home#/clusters. Make sure region is **'us-east-1'**.

   5. In the 'Configure Cluster' enter the information as shown in the image below. Name of the cluster in this example in **'gmesh-cluster'**.
   ![Create Cluster](./images/create_cluster_01.PNG)

   6. In the 'Specify networking' page, select the VPC created in earlier step. Leave the other settings as default.
   ![Specify Networking](./images/create_cluster_02.PNG)

   7. In the 'Configure Logging' page, leave all settings as default.
   ![Configure Logging](./images/create_cluster_03.PNG)

   8. In the 'Review and create' page, check the settings and click 'Create'.
   ![Review and create](./images/create_cluster_04.PNG)

   9. It will take around 20 minutes to create the cluster.

1. Configure your computer to communicate with your cluster
   1. Create or update a kubeconfig file for your cluster.
    ```shell
   aws eks update-kubeconfig \
    --region us-east-1 \
    --name gmesh-cluster
   ```

   2. Test your configuration.
   ```shell
   kubectl get svc
   ```
   3. Output
   ```
   NAME             TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
   svc/kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   1m
   ```

1. Create an IAM OpenID Connect (OIDC) provider

    1. Select the Configuration tab.
    2. In the Details section, copy the value for OpenID Connect provider URL.
    ![Details](./images/openid_01.PNG)
    3. Open the IAM console at https://console.aws.amazon.com/iam/.
    4. In the navigation panel, choose Identity Providers.
    5. Choose Add Provider.
    6. For Provider Type, choose OpenID Connect.
    7. For Provider URL, paste the OIDC provider URL for your cluster from step two, and then choose Get thumbprint.
    8. For Audience, enter sts.amazonaws.com and choose Add provider.
    ![Add provider](./images/openid_02.PNG)

1. Create Nodes

   1. Item 3a
      1. In the file named [cni-role-trust-policy.json](cni-role-trust-policy.json), replace **<111122223333>** (including <>) with your account ID and replace **XXXXXXXXXX45D83924220DC4815XXXXX** with the value after the last **/** of your OpenID Connect provider URL.
      2. Create an IAM role for the Amazon VPC CNI plugin.
      ```shell
	  aws iam create-role \
        --role-name gmeshEKSCNIRole \
        --assume-role-policy-document file://"cni-role-trust-policy.json"
      ```
      3. Attach the required Amazon EKS managed IAM policy to the role.
      ```shell
	  aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy \
        --role-name gmeshEKSCNIRole
      ```
   2. Associate the Kubernetes service account used by the VPC CNI plugin to the IAM role. Replace <111122223333> (including <>) with your account ID.
   ```shell
    aws eks update-addon \
      --cluster-name gmesh-cluster \
      --addon-name vpc-cni \
      --service-account-role-arn arn:aws:iam::<111122223333>:role/gmeshEKSCNIRole
   ```
   3. Create a node IAM role and attach the required Amazon EKS IAM managed policy to it.
      1. Create the node IAM role.
      ```shell
      aws iam create-role \
        --role-name gmeshEKSNodeRole \
        --assume-role-policy-document file://"node-role-trust-policy.json"
      ```
      2. Attach the required Amazon EKS managed IAM policies to the role.
      ```shell
      aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy \
        --role-name gmeshEKSNodeRole
      aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly \
        --role-name gmeshEKSNodeRole
      ```
    4. Open the Amazon EKS console at https://console.aws.amazon.com/eks/home#/clusters.
    5. Choose the name of the cluster that you created in Step 1: Create your Amazon EKS cluster, such as **gmesh-cluster**.
    6. Select the Configuration tab.
    7. On the Configuration tab, select the Compute tab, and then choose Add Node Group.
    8. On the Configure node group page, fill out the parameters accordingly, accept the remaining default values, and then choose Next. In this example, Name is **ng-gmesh** and IAM Role should be **gmeshEKSNodeRole**.
    ![Configure Node Group](./images/nodes_01.PNG)
    9. On the Set compute and scaling configuration page, accept the default values and select Next.
    ![Compute and Scaling](./images/nodes_02.PNG)
    10. On the Specify networking page, select an existing key pair to use for SSH key pair and then choose Next. If you don't have a key pair, you can create one with the following command. 
    ```shell
    aws ec2 create-key-pair --region us-east-1 --key-name gmeshKeyPair
    ```
    ![Specify networking](./images/nodes_03.PNG)
    11. On the Review and create page, review your managed node group configuration and choose Create.
    ![Review and create](./images/nodes_04.PNG)
    12. After several minutes, the Status in the Node Group configuration section will change from Creating to Active. Don't continue to the next step until the status is Active.


1. View resources

   1. In the left pane, select Clusters, and then in the list of Clusters, select the name of the cluster that you created, such as **gmesh-cluster**.
   2. On the Overview tab, you see the list of Nodes that were deployed for the cluster. You can select the name of a node to see more information about it.
   ![View Nodes](./images/view_resources_01.PNG)
   3. On the Workloads tab of the cluster, you see a list of the workloads that are deployed by default to an Amazon EKS cluster. You can select the name of a workload to see more information about it.


## Amazon EBS CSI driver
Gentics mentions that EFS is not supported, so we need to create a EBS driver.
Reference: https://docs.aws.amazon.com/eks/latest/userguide/ebs-csi.html

1. Create an IAM policy that allows the CSI driver's service account to make calls to AWS APIs on your behalf.
Download the IAM policy document:
```shell
curl -o example-iam-policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-ebs-csi-driver/v0.9.0/docs/example-iam-policy.json
```
Create the policy:
```shell
aws iam create-policy --policy-name AmazonEKS_EBS_CSI_Driver_Policy \
  --policy-document file://example-iam-policy.json
```
2. Create an IAM role and attach the IAM policy to it.
   1. View your cluster's OIDC provider URL. Replace <cluster_name> (including <>) with your cluster name.
   ```shell
   aws eks describe-cluster --name gmesh-cluster --query "cluster.identity.oidc.issuer" --output text
   ```
   ```
   Output:
   https://oidc.eks.us-west-2.amazonaws.com/id/XXXXXXXXXX45D83924220DC4815XXXXX
   ```
   2. Create an IAM role and attach the IAM policy to it. In the [trust-policy.json](trust-policy.json), replace <AWS_ACCOUNT_ID> (including <>) with your account ID and <XXXXXXXXXX45D83924220DC4815XXXXX> with the value returned in the previous step. 
   ```shell
   aws iam create-role \
    --role-name AmazonEKS_EBS_CSI_DriverRole \
    --assume-role-policy-document file://"trust-policy.json"
   ```
   3. Attach the IAM policy to the role. Replace <AWS_ACCOUNT_ID> (including <>) with your account ID. 
   ```shell
   aws iam attach-role-policy \
    --policy-arn arn:aws:iam::<AWS_ACCOUNT_ID>:policy/AmazonEKS_EBS_CSI_Driver_Policy \
    --role-name AmazonEKS_EBS_CSI_DriverRole
   ```
3. Deploy the driver.
```shell
kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
```
4. Annotate the ebs-csi-controller-sa Kubernetes service account with the ARN of the IAM role that you created previously. Replace the <AWS_ACCOUNT_ID> (including <>) with your account ID.
```shell
kubectl annotate serviceaccount ebs-csi-controller-sa \
  -n kube-system \
  eks.amazonaws.com/role-arn=arn:aws:iam::<AWS_ACCOUNT_ID>:role/AmazonEKS_EBS_CSI_DriverRole
```
5. Delete the driver pods. They're automatically redeployed with the IAM permissions from the IAM policy assigned to the role. 
```shell
kubectl delete pods \
  -n kube-system \
  -l=app=ebs-csi-controller
```
6. You'd need a volume of size 50GB or more. If it doesn't exist, create one.
 ![Create Volume](./images/create_volume_01.png)
7. Apply the stativ volume provisioning. In [ebs_static_provisioning.yml](ebs_static_provisioning.yml), set the correct **volumeHandle**. You can get it from https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Volumes


```shell
kubectl apply -f ebs_static_provisioning.yml
```

## Setup ECR to host the customized Gentics Mesh Docker image
Refer the official documentation to create a ECR: https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html

1. Authenticate to your default registry. Replace the 'aws_account_id' with your account ID.
```shell
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <aws_account_id>.dkr.ecr.us-east-1.amazonaws.com
```
2. Create a repository
```shell
aws ecr create-repository \
    --repository-name mesh \
    --image-scanning-configuration scanOnPush=true \
    --region us-east-1
```

3. Get the public token from Cognito and copy the contents to [public-keys.json](public-keys.json).
```shell
https://cognito-idp.{region}.amazonaws.com/{userPoolId}/.well-known/jwks.json
```

4. Build Docker image
```shell
docker build -t aws_account_id.dkr.ecr.us-east-1.amazonaws.com/mesh:1.7.6
```

5. Push the image to ECR
```shell
docker push aws_account_id.dkr.ecr.us-east-1.amazonaws.com/mesh:1.7.6
```

## Gentics Mesh Deployment on EKS Cluster
1. Apply the deployment file
```shell
kubectl apply -f gmesh_deployment.yaml
```
2. Check the public of the deployed service
```shell
kubectl get svc gmesh
```
```
Output:

NAME    TYPE           CLUSTER-IP     EXTERNAL-IP                                                               PORT(S)          AGE
gmesh   LoadBalancer   10.100.31.95   a2e8a6c21e55d401290fef5d8377fba0-1539196851.us-east-1.elb.amazonaws.com   8080:31220/TCP   150m
```
3. The address in the EXTERNAL-IP column is the publicly exposed IP.
4. If everything works fine you should be able to access the service at **EXTERNAL-IP:8080**.
5. Login with username **admin** and password **admin**.


## Seeding Schemas to Gentics
#### **NOTE: This step is only needed if you need to apply any changes to the deployed Schemas**

We are using [Mesh Seeding Plugin](https://github.com/savantly-net/mesh-seeding-plugin) for seeding OneStepRelief Schemas to Gentics. This plugin will run on the first start of Gentics Mesh and setup the necessary **Projects**, **Schemas**, **Microschemas**, **Nodes**, **Roles** and **Groups**.

In case if there are any changes made to the schema a redeployment of the 'gmesh' Pods will be necessary. Since the old data is still available in the EC2 Volume, applying a new Schema might cause some conflicts. In this case the following steps need to performed to apply the changed schemas properly:

1. Delete the **gmesh** deployment
```shell
kubectl delete deployment gmesh
```

2. You'd need to create a new volume of size 50GB or more.
 ![Create Volume](./images/create_volume_01.png)

3. Apply the stativ volume provisioning. In [ebs_static_provisioning.yml](ebs_static_provisioning.yml), set the correct **volumeHandle**. You can get it from https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Volumes

```shell
kubectl apply -f ebs_static_provisioning.yml
```

4. Build Docker image
```shell
docker build -t aws_account_id.dkr.ecr.us-east-1.amazonaws.com/mesh:1.7.6
```

5. Push the image to ECR
```shell
docker push aws_account_id.dkr.ecr.us-east-1.amazonaws.com/mesh:1.7.6
```

6. Apply the **gmesh** deployment
```shell
kubectl apply -f gmesh_deployment.yaml
```
