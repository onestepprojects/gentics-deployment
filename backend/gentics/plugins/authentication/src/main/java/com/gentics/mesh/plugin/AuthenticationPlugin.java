package com.gentics.mesh.plugin;

import java.io.File;
import java.nio.file.Files;
import java.util.*;

import com.gentics.mesh.etc.config.MeshOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import org.pf4j.PluginWrapper;

import com.gentics.mesh.core.rest.group.GroupResponse;
import com.gentics.mesh.core.rest.role.RoleReference;
import com.gentics.mesh.core.rest.role.RoleResponse;
import com.gentics.mesh.core.rest.user.UserUpdateRequest;
import com.gentics.mesh.plugin.auth.AuthServicePlugin;
import com.gentics.mesh.plugin.auth.MappingResult;
import com.gentics.mesh.plugin.env.PluginEnvironment;

import io.reactivex.Completable;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;


public class AuthenticationPlugin extends AbstractPlugin implements AuthServicePlugin {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationPlugin.class);
    private Set<JsonObject> publicKeys = Collections.emptySet();

    public AuthenticationPlugin(PluginWrapper wrapper, PluginEnvironment env) {
        super(wrapper, env);
    }

    @Override
    public Set<JsonObject> getPublicKeys() {
        return publicKeys;
    }

    @Override
    public Completable initialize() {
        return Completable.fromAction(() -> {
            log.info("Initializing Authentication Plugin");
            this.publicKeys = loadPublicKeys();
        });
    }

    @Override
    public MappingResult mapToken(HttpServerRequest req, String uuid, JsonObject token) {
        MappingResult result = new MappingResult();

        if (uuid == null) {
            log.info("First time login of the user");
        } else {
            log.info("Already synced user is logging in.");
        }

        log.info("Mapping user in plugin");
        printToken(token);
        String username = token.getString("preferred_username");
        UserUpdateRequest user = new UserUpdateRequest();
        user.setUsername(username);
        user.setEmailAddress(token.getString("email"));
        user.setFirstname(token.getString("name"));
        //user.setLastname(token.getString("name"));
        result.setUser(user);

        log.info("Mapping groups in plugin");
        List<GroupResponse> groupList = new ArrayList<>();
        try {
            JsonArray cognitoGroups = token.getJsonArray("cognito:groups");
            if (cognitoGroups == null) {
                throw new Exception("Cognito group information is not found in the token.");
            }
            int i = 0;
            while (true) {
                if (i >= cognitoGroups.size()) {
                    break;
                }

                String groupName = cognitoGroups.getString(i);
                log.info("Groups found: " + groupName);
                groupList.add(new GroupResponse().setName(groupName));
                ++i;
            }
            result.setGroups(groupList);
        }
        catch (Exception ex) {
            log.error("Error in assigning group. No group will be assigned to the user.", ex);
        }

        return result;
    }

    private File getPublicKeysFile() {
        String publicKeysPath = this.environment().options().getAuthenticationOptions().getPublicKeysPath();
        log.info("Public keys path from options: " + publicKeysPath);
        return new File(publicKeysPath);
    }

    @Override
    public boolean acceptToken(HttpServerRequest httpServerRequest, JsonObject token) {
        log.info("Checking token. Accepting..");
        printToken(token);
        return true;
    }

    private void printToken(JsonObject token) {
        String username = token.getString("preferred_username");
        System.out.println("Token for {" + username + "}");
        System.out.println(token.encodePrettily());
    }

    /**
     * Load the public keys from file.
     *
     * @return
     */
    private Set<JsonObject> loadPublicKeysFromFile() {
        try {
            Set<JsonObject> jwks = new HashSet();

            log.info("Looking for public key file: " + getPublicKeysFile().getPath());
            if (Files.exists(getPublicKeysFile().toPath())) {
                log.info("Found public key file..");
                String jsonStr = new String(Files.readAllBytes(getPublicKeysFile().toPath()));
                JsonObject json = new JsonObject(jsonStr);
                JsonArray keys = json.getJsonArray("keys");

                int i = 0;
                while(true) {
                    if (i >= keys.size()) {
                        break;
                    }

                    JsonObject key = keys.getJsonObject(i);
                    jwks.add(key);
                    ++i;
                }

                if (log.isInfoEnabled()) {
                    for (JsonObject key : jwks) {
                        log.info("Loaded public key {\n" + key.encodePrettily() + "\n}");
                    }
                }
            }
            return jwks;
        } catch (Exception e) {
            log.error("Error while loading public keys", e);
        }
        return Collections.emptySet();
    }

    /**
     * Load the public keys from environment.
     *
     * @return
     */
    private Set<JsonObject> loadPublicKeys() {
        try {
            Set<JsonObject> jwks = new HashSet();
            log.info("Looking for public key in environment options... ");

            List<JsonObject> publicKeyFromEnv = this.environment().options().getAuthenticationOptions().getPublicKeys();
            for (JsonObject publicKey : publicKeyFromEnv) {
                jwks.add(publicKey);
            }

            if (log.isInfoEnabled()) {
                for (JsonObject key : jwks) {
                    log.info("Loaded public key {\n" + key.encodePrettily() + "\n}");
                }
            }

            return jwks;
        } catch (Exception e) {
            log.error("Error while loading public keys", e);
        }
        return Collections.emptySet();
    }
}
