package com.gentics.mesh.plugin;

import java.util.HashMap;

public class Ratings {

	private Double avgRating = 0.0;

    private HashMap<String, Integer> ratings = new HashMap<String, Integer>();

	public Double getAvgRating() {
		return avgRating;
	}

    public HashMap<String, Integer> getRatings() {
        if (ratings == null) {
            ratings = new HashMap<String, Integer>();
        }

		return ratings;
	}

    public void calculateAvgRating() {

        Integer validRatings = 0;
        Double totalRatings = 0.0;

        for (HashMap.Entry<String, Integer> pair: ratings.entrySet()) {
            if (pair.getValue() == 0) {
                continue;
            }

            validRatings += 1;
            totalRatings += pair.getValue();
        }

        if (validRatings == 0) {
            avgRating = 0.0;
        } else {
            avgRating = totalRatings / validRatings;
        }
    }
}
