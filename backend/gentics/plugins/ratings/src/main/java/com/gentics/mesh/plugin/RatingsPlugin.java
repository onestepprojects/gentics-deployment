package com.gentics.mesh.plugin;

import java.io.File;

import com.google.gson.*;
import org.pf4j.PluginWrapper;

import com.gentics.mesh.FieldUtil;
import com.gentics.mesh.core.rest.node.NodeUpdateRequest;
import com.gentics.mesh.core.rest.project.ProjectCreateRequest;
import com.gentics.mesh.plugin.env.PluginEnvironment;
import com.gentics.mesh.rest.client.MeshRestClient;
import static com.gentics.mesh.FieldUtil.createStringField;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.reactivex.core.buffer.Buffer;

public class RatingsPlugin extends AbstractPlugin implements RestPlugin {

    private static final Double version = 1.2;

    private static final String ratingsFiledName = "ratings";

    private static final Logger log = LoggerFactory.getLogger(RatingsPlugin.class);

    public RatingsPlugin(PluginWrapper wrapper, PluginEnvironment env) {
        super(wrapper, env);
    }

    @Override
    public Completable initialize() {
        return Completable.fromAction(() -> {
            log.info(String.format("Ratings plugin started. Version: %s", version));
        });
    }

    @Override
    public Router createGlobalRouter() {
        Router router = Router.router(vertx());

        // Retrieve Version
        // Path: /api/v2/plugins/ratings/version
        router.route("/version").handler(rc -> {
            PluginContext context = wrap(rc);
            context.client().me().toSingle().subscribe(me -> {
                log.info("retrieveVersion for: " + version);

                rc.response().putHeader("content-type", "application/json");
                rc.response().end(String.format("{\"version\":%s}", version));
            }, rc::fail);
        });

        return router;
    }

    @Override
    public Router createProjectRouter() {
        Router router = Router.router(vertx());

        // Retrieve Ratings
        router.getWithRegex("/([a-zA-Z0-9_.-]*)").handler(this::retrieveRatings).failureHandler(rc -> {
            log.error("Error while retrieving ratings", rc.failure());
            rc.response().setStatusCode(500).end();
        });

        // Update Ratings
        router.postWithRegex("/([a-zA-Z0-9_.-]*)\\/([0-5])").handler(this::updateRatings).failureHandler(rc -> {
            log.error("Error while updating ratings", rc.failure());
            rc.response().setStatusCode(500).end();
        });

        return router;
    }

    private void retrieveRatings(RoutingContext rc) {
        PluginContext context = wrap(rc);
        String uuid = rc.request().params().get("param0");
        log.info("retrieveRatings for: " + uuid);
        context.client().me().toSingle().subscribe(me -> {
            context.client().findNodeByUuid(context.projectName(), uuid).toSingle().subscribe(node -> {
                Double avgRating = 0.0;
                int userRating = 0;
                if (node.getFields().getStringField(ratingsFiledName) != null) {
                    String data = node.getFields().getStringField(ratingsFiledName).getString();
                    Gson gson = new Gson();
                    try {
                        Ratings ratings = gson.fromJson(data, Ratings.class);
                        avgRating = ratings.getAvgRating();
                        if (ratings.getRatings().containsKey(me.getUsername())) {
                            userRating = ratings.getRatings().get(me.getUsername());
                        }
                    } catch (JsonParseException e) {
                        log.info("Error trying to deserialize rating with:\n" + data + "\n" + e);
                    }
                }

                rc.response().putHeader("content-type", "application/json");
                rc.response().end(String.format("{\"avgRating\":%s,\"userRating\":%s}", avgRating, userRating));
            }, rc::fail);
        }, rc::fail);
    }

    private void updateRatings(RoutingContext rc) {
        PluginContext context = wrap(rc);
        String uuid = rc.request().params().get("param0");
        final Integer newRate = Integer.parseInt(rc.request().params().get("param1"));
        log.info("updateRatings for: " + uuid + " newRate: " + newRate);
        context.client().me().toSingle().subscribe(me -> {
            context.client().findNodeByUuid(context.projectName(), uuid).toSingle().subscribe(node -> {

                Gson gson = new Gson();
                Ratings ratings = null;
                if (node.getFields().getStringField(ratingsFiledName) != null) {
                    String data = node.getFields().getStringField(ratingsFiledName).getString();
                    try {
                        ratings = gson.fromJson(data, Ratings.class);
                    } catch (JsonParseException e) {
                        log.info("Error trying to deserialize rating with:\n" + data + "\n" + e);
                    }
                }

                if (ratings == null) {
                    ratings = new Ratings();
                    ratings.getRatings().put("Void", 0);
                }

                ratings.getRatings().put(me.getUsername(), newRate);
                ratings.calculateAvgRating();

                NodeUpdateRequest update = node.toRequest();
                update.setVersion(node.getVersion());
                update.setLanguage(node.getLanguage());
                update.getFields().put(ratingsFiledName, FieldUtil.createStringField(gson.toJson(ratings)));

                final Double newAvgRating = ratings.getAvgRating();
                context.client().updateNode(context.projectName(), uuid, update).toSingle().subscribe(updateItem -> {
                    context.client().publishNode(context.projectName(), uuid).toSingle().subscribe(publishItem -> {
                        rc.response().putHeader("content-type", "application/json");
                        rc.response().end(String.format("{\"avgRating\":%s,\"userRating\":%s}", newAvgRating, newRate));
                    }, rc::fail);
                }, rc::fail);
            }, rc::fail);
        }, rc::fail);
    }

    @Override
    public String restApiName() {
        return "ratings";
    }
}
