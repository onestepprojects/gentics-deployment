# gentics-deployment

Project is strictly for deploying Gentics Mesh

## Relevant Gentics Documentation

[Building Blocks](https://getmesh.io/docs/building-blocks/): General introduction to core concepts of Gentics Mesh

[API Introduction](https://getmesh.io/docs/guides/mesh-api-intro/)

[API Specification](https://getmesh.io/docs/api/)

## Deploying Gentics Locally

Gentics can be deployed locally with Docker. Make sure you have the [Docker](https://docs.docker.com/get-started/) installed before continuing.

From inside the `deployment/gentics` directory, run the following:

```bash
docker build -t mesh -f Dockerfile.dev .
docker run --rm --name mesh -p 8080:8080 mesh
```

This will run gentics locally. Data is only stored in-memory, so be warned that closing down the docker container will result in losing anything you have stored in gentics. You may use our gentics instance running in AWS (documented [here](https://docs.google.com/document/d/1L2Gab7uoMfHXKxGgH9tipTi6giKTlhMncnyZvabbdHg/edit)) for anything you want to stick around.

Go to http://localhost:8080, and enter the username/password `admin:admin`. Gentics will ask you to supply a new password.

Click "Admin" in the upper-right corner and then "Groups". Click the x next to "anonymous" to delete that role and replace it with the admin role. This will allow you to perform actions without authentication (for now).

## Interacting with Gentics

The example project should be enough to get you started. It has a single page with a form at the top for creating/viewing a discussion board, given a name and id. Only the id is needed in order to find a discussion board.

Your frontend project will require the `dotenv` package as a dev dependency. In the `.env` file, set the variable `REACT_APP_GENTICS_API_URL` to either your local url or the aws instance. You should be able to copy the `services/` directory and include it in your project. Then follow the `App.js` file to use the Discussion Board appropriately.

You may also change the project name (fund, resources, etc) in `services/api/api.js`.

Please reach out to Michaela DeForest if you have any questions.
